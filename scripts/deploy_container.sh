#!/bin/ash

# Exit on any non zero return value
set -ex

# Update the task definition with a new image and perform a rolling update to the service
#
# ECS_CLUSTER  - Name of the ECS cluster, usually the group name and environment like micro-staging
# TASK_NAME    - Name of the Task running in ECS, like manifold-api-staging
# IMAGE_TAG    - Tag to apply to the image, like semver tag or commit hash
# SKIP_SERVICE - Whether or not to update the task definition but skip updating the service. Used when a task runs without a service like cron or short lived tasks.
#
# Example:
# deploy_container.sh manifold-staging keycloak-staging 1.0.0
#
# Or in Gitlab CI pipeline
# deploy_container.sh manifold-staging keycloak-staging $CI_COMMIT_TAG


# Originally from https://github.com/aws/aws-sdk/issues/406#issuecomment-1314183221

ECS_CLUSTER=$1
TASK_NAME=$2
TAG=$3
SKIP_SERVICE=$4


TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$TASK_NAME" --region us-east-1)
IMAGE=$(echo $TASK_DEFINITION | jq -r '.taskDefinition | .containerDefinitions[0].image')

NEW_IMAGE=$(echo $IMAGE | sed -e "s/:.*/:$TAG/")
NEW_TASK_DEFINITION=$(echo "$TASK_DEFINITION" | jq --arg IMAGE "$NEW_IMAGE" '.taskDefinition | .containerDefinitions[0].image = $IMAGE | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt) | del(.registeredBy)')
NEW_TASK_INFO=$(aws ecs register-task-definition --region us-east-1 --cli-input-json "$NEW_TASK_DEFINITION")
NEW_REVISION=$(echo $NEW_TASK_INFO | jq '.taskDefinition.revision')

if [ "$SKIP_SERVICE" == "skip_service" ]; then
  echo "Skipping service update"
else
  aws ecs update-service --cluster ${ECS_CLUSTER} --service ${TASK_NAME} --task-definition ${TASK_NAME}:${NEW_REVISION} --region us-east-1

  aws ecs wait services-stable --cluster ${ECS_CLUSTER} --services ${TASK_NAME} --region us-east-1
fi

echo "Deployment complete!"
exit 0
