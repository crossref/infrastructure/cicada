#!/bin/ash

# Exit on any non zero return value
set -ex

# Create a manifest for a single architecture like amd64 or arm64 and push it to AWS Public Gallery
#
# IMAGE_NAME   - Name of the image, usually the application name
# IMAGE_TAG    - Tag to apply to the image, like semver tag or commit hash
# ARCH_NAME    - Name of the single architecture build (amd64, arm64)
#
# Example:
# create_singlearch_manifest.sh manifold 1.0.0 amd64
#
# Or in Gitlab CI pipeline
# create_singlearch_manifest.sh manifold $CI_COMMIT_TAG amd64

IMAGE_NAME=$1
IMAGE_TAG=$2
ARCH_NAME=$3

export AWS_DEFAULT_PROFILE="portal"

aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws

docker manifest create public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG --amend public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG-$ARCH_NAME
docker manifest push public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG


# The fargate task definitions get created by terraform and default to using latest. Then we use 
# the deployment scripts in this image to update the service to the correct tag name during a deployment. 
#
# Terraform is set to ignore changes to the task definition so it doesnt see these subsequent updates.
# But if you change anything in the task definition in terraform like ports, CPU/MEM etc., it will almost 
# always destroy and recreate the task definition. This also means that the image tag gets changed 
# from whatever was last deployed back to latest.
#
# Because of this, we need to always update the latest tag to whatever tag name was just created. Otherwise 
# a terraform update might break the currently deployed tasks.
#
# Preferably, we could get terraform to not always use latest and instead have it figure out the most 
# recently deployed tag and recreate the task definition using that. This could be done by using a data
# source of the task definition revision and falling back to a default value of latest. But that isnt 
# possible in terraform right now.

docker manifest create public.ecr.aws/crossref/$IMAGE_NAME:latest --amend public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG-$ARCH_NAME
docker manifest push public.ecr.aws/crossref/$IMAGE_NAME:latest
