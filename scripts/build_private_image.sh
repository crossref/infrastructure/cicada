#!/bin/ash

# Exit on any non zero return value
set -ex

# Build a container image for amd64 only and push it to AWS private ECR
#
# ACCOUNT_ID   - AWS account ID where images will be stored in ECR
# DOCKERFILE   - Path to the Dockerfile
# IMAGE_NAME   - Name of the image, usually the application name
# IMAGE_TAG    - Tag to apply to the image, like commit hash, semver or latest
# CONTEXT      - Path to the build context, usually ./ which is the root of the git repo
#
# Example:
# build_private_image.sh $aws_account_id "Dockerfile" manifold 1.0.0 "./"
#
# Or in Gitlab CI pipeline
# build_private_image.sh $AWS_ACCOUNT_ID_PORTAL "${CI_PROJECT_DIR}/Dockerfile" manifold $CI_COMMIT_TAG $CI_PROJECT_DIR

ACCOUNT_ID=$1
DOCKERFILE=$2
IMAGE_NAME=$3
IMAGE_TAG=$4
CONTEXT=$5

export AWS_DEFAULT_PROFILE="portal"

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com

docker build --pull -f $DOCKERFILE -t $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG-amd64 $CONTEXT
docker push $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG-amd64

docker manifest create $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG --amend $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG-amd64
docker manifest push $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG


# The fargate task definitions get created by terraform and default to using latest. Then we use 
# the deployment scripts in this image to update the service to the correct tag name during a deployment. 
#
# Terraform is set to ignore changes to the task definition so it doesnt see these subsequent updates.
# But if you change anything in the task definition in terraform like ports, CPU/MEM etc., it will almost 
# always destroy and recreate the task definition. This also means that the image tag gets changed 
# from whatever was last deployed back to latest.
#
# Because of this, we need to always update the latest tag to whatever tag name was just created. Otherwise 
# a terraform update might break the currently deployed tasks.
#
# Preferably, we could get terraform to not always use latest and instead have it figure out the most 
# recently deployed tag and recreate the task definition using that. This could be done by using a data
# source of the task definition revision and falling back to a default value of latest. But that isnt 
# possible in terraform right now.

docker manifest create $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:latest --amend $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:$IMAGE_TAG-amd64
docker manifest push $ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com/$IMAGE_NAME:latest
