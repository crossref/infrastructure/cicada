#!/bin/ash

# Exit on any non zero return value
set -ex

# Build a container image for the specified CPU architecture and push it to AWS Public Gallery
#
# ARCHITECTURE - CPU architecture. One of amd64 or arm64.
# DOCKERFILE   - Path to the Dockerfile
# IMAGE_NAME   - Name of the image, usually the application name
# IMAGE_TAG    - Tag to apply to the image, like semver tag or commit hash
# CONTEXT      - Path to the build context, usually the root of the git repo
#
# Example:
# build_image.sh amd64 Dockerfile manifold 1.0.0 "./"
#
# Or in Gitlab CI pipeline
# build_image.sh amd64 "${CI_PROJECT_DIR}/Dockerfile" manifold $CI_COMMIT_TAG $CI_PROJECT_DIR

ARCHITECTURE=$1
DOCKERFILE=$2
IMAGE_NAME=$3
IMAGE_TAG=$4
CONTEXT=$5

export AWS_DEFAULT_PROFILE="portal"

aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws

docker build --pull -f $DOCKERFILE -t public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG-$ARCHITECTURE $CONTEXT
docker push public.ecr.aws/crossref/$IMAGE_NAME:$IMAGE_TAG-$ARCHITECTURE
