FROM public.ecr.aws/docker/library/docker:24.0.5-dind-alpine3.18

# AWS Account IDs
ARG AWS_ACCOUNT_ID_STAGING
ARG AWS_ACCOUNT_ID_SANDBOX
ARG AWS_ACCOUNT_ID_RESEARCH
ARG AWS_ACCOUNT_ID_PRODUCTION
ARG AWS_ACCOUNT_ID_PORTAL

# Configure AWS credentials
RUN mkdir ~/.aws/ \
 && echo -e "[staging]\nrole_arn = arn:aws:iam::$AWS_ACCOUNT_ID_STAGING:role/gitlab-iam-role\ncredential_source = Ec2InstanceMetadata\n" > ~/.aws/credentials \
 && echo -e "[sandbox]\nrole_arn = arn:aws:iam::$AWS_ACCOUNT_ID_SANDBOX:role/gitlab-iam-role\ncredential_source = Ec2InstanceMetadata\n" >> ~/.aws/credentials \
 && echo -e "[research]\nrole_arn = arn:aws:iam::$AWS_ACCOUNT_ID_RESEARCH:role/gitlab-iam-role\ncredential_source = Ec2InstanceMetadata\n" >> ~/.aws/credentials \
 && echo -e "[production]\nrole_arn = arn:aws:iam::$AWS_ACCOUNT_ID_PRODUCTION:role/gitlab-iam-role\ncredential_source = Ec2InstanceMetadata\n" >> ~/.aws/credentials \
 && echo -e "[portal]\nrole_arn = arn:aws:iam::$AWS_ACCOUNT_ID_PORTAL:role/gitlab_runner_portal\ncredential_source = Ec2InstanceMetadata\n" >> ~/.aws/credentials

# Install common tools
RUN echo 'https://dl-cdn.alpinelinux.org/alpine/v3.18/community' >> /etc/apk/repositories \
 && apk add --no-cache \
    aws-cli \
    jq \
    npm \
    git \
    curl \
    python3-dev \
    py3-pip \
    gcc \
 && rm -rf /var/cache/apk/*

# Install semantic-release. Used to generate application version tags.
RUN npm install -g semantic-release@20 @semantic-release/gitlab @semantic-release/exec @semantic-release/changelog @semantic-release/npm @semantic-release/git

# Copy scripts, set permissions and install dependencies
COPY scripts/* /usr/local/bin/
RUN chmod 755 /usr/local/bin/*.sh

