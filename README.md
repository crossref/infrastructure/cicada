# Cicada

Container image to be used for CI/CD on Gitlab

Can be used for:

- Building multiarch container images
- Pushing container images to AWS Public Gallery
- Pushing container images to AWS ECR private repositories
- Deploying new images to tasks on AWS ECS clusters

## Building images for multiple architectures

Example:

```
build_amd64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  script:
    - build_image.sh amd64 "${CI_PROJECT_DIR}/Dockerfile" keycloak $CI_COMMIT_TAG $CI_PROJECT_DIR
  only:
    - tags

build_arm64_image:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - arm64
  services:
    - docker:dind
  script:
    - build_image.sh arm64 "${CI_PROJECT_DIR}/Dockerfile" keycloak $CI_COMMIT_TAG $CI_PROJECT_DIR
  only:
    - tags

multi_arch_manifest:
  image: public.ecr.aws/crossref/cicada:latest
  stage: build
  tags:
    - aws
    - ec2
    - amd64
  services:
    - docker:dind
  needs: ['build_amd64_image', 'build_arm64_image']
  script:
    - create_manifest.sh keycloak $CI_COMMIT_TAG
  only:
    - tags
```

## Deploying new images to ECS

Example:

```
deploy_app_staging:
  image: public.ecr.aws/crossref/cicada:latest
  stage: deploy
  tags:
    - aws
    - ec2
    - amd64
  variables:
    AWS_DEFAULT_PROFILE: staging
  script:
    - deploy_container.sh manifold-staging keycloak-staging $CI_COMMIT_TAG
  only:
    - tags
```

## Building an image for a single architecture

Its preferable to always build multiarch images so that we can use the image for both local dev and production. However, there are some cases where this might not work. Usually because an arm64 variant of an upstream image isnt available. In those cases, you can use the singlearch script instead.

You need to use the build_image.sh to build and then the create_singlearch_manifest.sh script to create a manifest only for that architecture.

```
create_singlearch_manifest.sh keycloak $CI_COMMIT_TAG amd64
```

## Cicada image bootstrapping

In order to push images to AWS Public Gallery we need to use the Cicada image. That presents a catch-22 if we want to have the Cicada image itself hosted on AWS Public Gallery. 

We first build an image using plain docker commands and push it to the Gitlab Registry. We then use that image from the Gitlab Registry to run docker build commands, build a multiarch image and push it to AWS Public Gallery. 
